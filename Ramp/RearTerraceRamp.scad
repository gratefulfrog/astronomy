$fn=100;


//////////// TARGETS  //////////////////////////////

// 2 required, no difference between left and right
//translate([-5,0,0])
//foreSection();
//translate([0,-SectionX,0])
//  foreSection();

leftAftSection();
//rightAftSection();


//////////// End TARGETS  //////////////////////////////
/// Version 02 tests:
// bolt length reduced to 16mm
// bolt head dia needs to be 0.5mm bigger, 
// for the wrench hole on the fore/afte bolts use diamter similar to bolt Dia
// Aft Crown dia needs to be 1mm bigger
// right aft wrench degagement needs to be reworked with a slot for 180deg movement
//   and removal of the slowed sections.

// used 0.98 Extrusion Factor to reduce bulging in the fill, as well asset infill to 95% in the slicer

/// Version 03 tests
// AFt holes for forward facing bolts do not have head cut out! 

// Version 04
// placed it at the back of the bed by accident with the slicer...

// Version 05
// extended the wrench hole for the right aft section; note EM for neon green abs is 1.0 not 0.98 in the slicer
// failed because of warping.... cut the blocks into just a few small sections !!
// raise bed temp
// get clamps for the magnetic sheet

// Version 06
// cut out sections to reduce filament usage and contraction stress on bed.


TEST = false;

SectionX = 250;
SectionXCutter = SectionX/6.;

RounderDia  = 1;

ForeY = -183;
ForeZ = 16.5;
                     
AftZ           = ForeZ;
AftY           = 132;
AftDepth       = -15;
AftDepthY      = 16;
AftInnerZ      = 25;
AftCrownHeight = 3.5;
AftRearZ       = 26;
AftTopY        = 17;
AftTopZ        = 13.5;
AftCrownDia    = 6.3; //5.3;

//m2.5 hex wrench
wrenchShortL    = 20;  // ext dim
wrendchLongL    = 90;  // ext dim
wrenchDia       = 3.6;
wrenchDgtAngle  = 70;

// Bolt
boltDia          = 3.2;
boltHeadDia      = 6; //5.5;
boltHeadLength   = 3;
boltLength       = 20;
boltDepthEpsilon = 3;

// Insert
insertHoleDia = 4.5;
insertLength  = 4;
insertFullDia = 5.1;


correctionWrenchAcces = 1.25;

foreBoltPositionVec=[[0,-SectionX/3.,ForeZ/2.-correctionWrenchAcces],
                     [0,-2*SectionX/3.,ForeZ/2.-correctionWrenchAcces]];
                     
extraForeBoltPosition = [0,-SectionX+10,ForeZ/2.-correctionWrenchAcces];
                     
leftBoltPositionVec=[[AftDepthY/2.,-SectionX,(AftRearZ/2.+AftDepth)/2.],
                     [3*AftDepthY/2.+AftTopY,-SectionX,(AftRearZ/2.+AftDepth)/2.]];
                     

//echo(leftBoltPositionVec);

module foreHull(){
 pointVec = [[0,0],
              [0,ForeZ],
              [ForeY,0]];
   hull(){
        polygon(pointVec);
        translate([ForeY,RounderDia/2])
          circle(d=RounderDia);
      }
}
//foreHull();      

module foreSectionRaw(){
  rotate([90,0,0])
    linear_extrude(SectionX)
      foreHull(); 
} 
//foreSectionRaw();

module foreSectionCutter(){
  rotate([90,0,0])
    linear_extrude(SectionXCutter)
      translate([0,-3,0])
        foreHull(); 
} 
//foreSectionCutter();

module foreSectionCutters(){
//translate([0,SectionX/1.5,0])
  for (yTrans = [-SectionX/12:-2*SectionX/6:-SectionX])
    translate([0,yTrans,0])
      foreSectionCutter();
}
//foreSectionCutters();

module foreSection(){
  difference(){
    foreSectionRaw();
    foreHoles(true);
    foreHoles(false);
    foreSectionCutters();
  }
}
//foreSection();

module foreHoles(bolts){
  xTrans = bolts ? boltLength+boltHeadLength-boltLength/2.-boltDepthEpsilon : 0;
  for (p = foreBoltPositionVec)
    translate(p)
      translate([xTrans,0,0])
        rotate([0,-90,0])
          if (bolts)
            bolt();
          else 
            insert();
            
}
//foreHoles(true);
//foreHoles(false);

module aftHoles(){
  xTrans = boltLength+boltHeadLength-boltLength/2.;
  posVec = TEST ? concat(foreBoltPositionVec,[extraForeBoltPosition]) :foreBoltPositionVec;
 echo(posVec); 
  for (p = posVec)
    translate(p){
      for (i=[0:3])
      translate([xTrans+i*boltHeadLength,0,0])
        rotate([0,-90,0])
          bolt();
      translate([boltLength/2.,0,0])
        wrenchHoleCutter(2*AftDepthY+AftTopY-boltLength/2.);
      }
}
//aftHoles();

module aftHull(){
  pointVec = [[0,0],
              [0,AftZ],
              [AftY,AftRearZ+AftDepth+RounderDia],
              [AftY,AftRearZ+AftDepth],
              [2*AftDepthY+AftTopY, AftRearZ+AftDepth],
              [2*AftDepthY+AftTopY,AftDepth],
              [AftDepthY+AftTopY,AftDepth],
              [AftDepthY+AftTopY,AftInnerZ+AftDepth],
              //[AftDepthY+AftTopY/2.,AftInnerZ+AftDepth+AftCrownHeight],
              [AftDepthY,AftInnerZ+AftDepth],
              [AftDepthY,AftDepth],
              [0,AftDepth],
              ];
  difference(){
        union(){
          polygon(pointVec);
          translate([AftY,RounderDia/2+AftRearZ+AftDepth])
            circle(d=RounderDia);
        }
        translate([AftDepthY+AftTopY/2.,AftInnerZ+AftDepth])
          circle(d=AftCrownDia);
     }
}
//aftHull();

module aftInnerHullCutter(){
  pointVec = [ /*
              [0,0],
              [0,AftZ],
              
              [AftY,AftRearZ+AftDepth+RounderDia],
              */
              //[AftY,AftRearZ+AftDepth],
              [2*AftDepthY+AftTopY, AftRearZ+AftDepth+2*RounderDia],
              [2*AftDepthY+AftTopY,AftDepth],
              [AftDepthY+AftTopY,AftDepth],
              [AftDepthY+AftTopY,AftRearZ+AftDepth+2*RounderDia],//AftInnerZ+AftDepth],
              //[AftDepthY+AftTopY/2.,AftInnerZ+AftDepth+AftCrownHeight],
              //[AftDepthY,AftInnerZ+AftDepth],
              //[AftDepthY,AftDepth],
              //[0,AftDepth],
              ];
  //difference(){
        union(){
          polygon(pointVec);
          //translate([AftY,RounderDia/2+AftRearZ+AftDepth])
            //circle(d=RounderDia);
        }
/*        translate([AftDepthY+AftTopY/2.,AftInnerZ+AftDepth])
          circle(d=AftCrownDia);
     } */
}
//aftInnerHullCutter();
/*
difference(){
aftHull();
aftInnerHullCutter();
}
*/
module aftSectionRaw(){
  rotate([90,0,0])
    linear_extrude(SectionX)
      aftHull();
}
//aftSectionRaw();

module aftSectionCutter(){
  rotate([90,0,0])
    linear_extrude(SectionXCutter)
      translate([0,-3,0])
        aftHull(); 
  } 
//aftSectionCutter();

module aftInnerSectionCutter(){
  rotate([90,0,0])
    linear_extrude(SectionXCutter)
      translate([0,-3,0])
        aftInnerHullCutter(); 
  } 
//aftInnerSectionCutter();

module aftSectionCutters(){
  for (yTrans = [-SectionX/12:-2*SectionX/6:-SectionX])
    translate([0,yTrans,0])
      aftSectionCutter();
}
//aftSectionCutters();

module aftInnerSectionCutters(){
  for (yTrans = [-3*SectionX/12:-2*SectionX/6:-0.6*SectionX])
    translate([0,yTrans,0])
      aftInnerSectionCutter();
}
//aftInnerSectionCutters();


module aftSection(){
  difference(){
    aftSectionRaw();
    aftHoles();
    aftSectionCutters();
    aftInnerSectionCutters();
  }
}
//aftSection();

module leftAftSection(){
  difference(){
    aftSection();
    leftAftHoles(true);
    leftAftHoles(false);
  }
}
//leftAftSection();

module rightAftSection(){
  difference(){
    translate([0,-SectionX,0])
      aftSection();
    rightWrenchDgtHoles();
    rightAftHoles();
  }
}
//rightAftSection();

module leftAftHoles(bolts){
  xTrans = bolts ? boltLength+boltHeadLength-boltLength/2.-boltDepthEpsilon : 0;
  for (p = leftBoltPositionVec)
    translate(p)
      translate([0,-xTrans,0])
        rotate([-90,0,0])
          if (bolts)
            bolt();
          else 
            insert();
            
}
//leftAftHoles(true);
//leftAftHoles(false);

module rightAftHoles(){
  xTrans = boltLength+boltHeadLength-boltLength/2.;
  for (p = leftBoltPositionVec)
    translate(p){
      translate([0,-xTrans,0])
        rotate([-90,0,0])
          bolt();
      }
}
//rightAftHoles();

module wrenchDgtRawOld(){
  h = AftDepthY/(2. * tan (wrenchDgtAngle/2.));
  x = AftDepthY;
  y = 2*x;
  rotate([90,0,0])
    linear_extrude(wrenchShortL+5)
      hull(){
        circle(d=boltHeadDia);
        translate([0,-h-y/2,0])
          square([x,y], center = true);
      }
}
//wrenchDgtRawOld();

module wrenchDgtRaw(){
  h = AftDepthY/(2. * tan (wrenchDgtAngle/2.));
  x = AftDepthY;
  y = 2*x;
  rotate([90,0,0])
    linear_extrude(wrenchShortL+15)
      hull(){
        circle(d=boltHeadDia);
        translate([0,-h-y/2,0])
          circle(d=boltHeadDia);
          //square([x,y], center = true);
      }
  translate([0,10/2.-(wrenchShortL+5),-y/2.+boltHeadDia/2.])
    cube([x,10,y], center =true);
}
//wrenchDgtRaw();

module rightWrenchDgtHoles(){
  xTrans = boltLength+boltHeadLength-boltLength/2.;
  for (p = leftBoltPositionVec)
    translate(p)
      translate([0,-xTrans,0])
        wrenchDgtRaw();
}
//rightWrenchDgtHoles();


module insert(){
  cylinder(h=insertLength,d=insertHoleDia);
}
//insert();

module bolt(){
  fulllength = boltLength+boltHeadLength;
  cylinder(h=fulllength,d=boltDia);
  cylinder(h=boltHeadLength,d=boltHeadDia);
  
}
//bolt();

module boltHeadHoleCutter(length){
  rotate([0,90,0])
    cylinder(h=length,d=boltHeadDia);
}
//boltHeadHoleCutter(20);

module wrenchHoleCutter(length){
  rotate([0,90,0])
    cylinder(h=length,d=wrenchDia);
}
//wrenchHoleCutter(20);

