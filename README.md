# Astronomy

# Telescope
- Telescope GSO 12" f/5 Dobsonian Deluxe version, GSD980, Teleskop Service, Parsdorf, Germany

# Eyepieces
- 30mm TSWA30 (GSO)
- 9mm TSSP9 (GSO)
- 8mm Hyperion 68° field (Clef Des Etoiles 2023 11)

# Eyepiece accessories
- Baader ND 0.6 filter for moon (25% transmission) (Clef Des Etoiles 2023 11)
- 35mm extension tube (use with 8mm eyepiece) (GSO)

# Pointers
- Telerad (Clef Des Etoiles 2024 04)
- MeMstar (Clef Des Etoiles 2024 04)

# Photo
- Tridaptor phone holder (moveshootmove.com 2023 12)

# Misc
- red head lamp (moveshootmove.com 2023 12)
- red torch (moveshootmove.com 2023 12)